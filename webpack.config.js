const { shareAll, withModuleFederationPlugin } = require('@angular-architects/module-federation/webpack');

module.exports = withModuleFederationPlugin({

  name: 'fe-persee-recouvrement',

  exposes: {
    './Module' : './src/app/components/recouvrement.module.ts',
    MenuEntry : './src/app/components/recouvrement-menu.json'
  },

  shared: {
    ...shareAll({ singleton: true, strictVersion: true, requiredVersion: 'auto' }),
  },

});
